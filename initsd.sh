#!/bin/bash

IP=$1

for module in `ls modules/*.js`; do
    echo -n "Uploading module: ${module}... "
    curl \
        -XPUT \
        --data-binary "@${module}" \
        http://${IP}/module/$(basename ${module})
done

NUM_ANIMATIONS="25"
[[ "$(command -v jq)" ]] && \
    NUM_ANIMATIONS=$(curl -s http://${IP}/anim | jq '.animations | length')

echo "Deleting ${NUM_ANIMATIONS} animations"
for i in `seq 0 ${NUM_ANIMATIONS}`; do
    curl -XDELETE http://${IP}/anim/$(printf "%03X" $i)
done

for anim in `ls animations/*.js`; do
    echo -n "Uploading animation: ${anim}... "
    curl -XPOST --data-binary "@${anim}" http://${IP}/anim
done

echo -n "Removing autostart animation... "
curl -XDELETE http://${IP}/autostart && echo "OK"
