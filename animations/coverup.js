/* Copyright (c) 2018 ColorBit LLC
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
var colors = require('colors.js');
var types = require('types.js');
var descriptions = require('desc.js');

module.exports = {
  init() {
    state.up = true;
    state.ledPos = 0;
    state.width = NUM_LEDS / params.Number_Of_Coverups;
    state.color = params.Color[0];
    state.currentColor = 0;
    state.lastColor = params.Color.length;
    state.changeColor = false;
  },

  anim() {
    var width = state.width;
    var Random_Color = params.Random_Color;
    var Every_LED_Random_Color = params.Every_LED_Random_Color;

    LEDS[state.ledPos] = state.color;
    mirror(LEDS, width);
    show(LEDS);
    delay(params.Animation_Speed);

    if (state.up) {
      state.ledPos += 1;
    } else {
      state.ledPos -= 1;
    }

    if (params.Wrap_Around) {
      if (state.ledPos >= width) {
        state.ledPos = 0;
        state.changeColor = true;
      }
    } else if (state.ledPos >= width) {
      state.up = false;
      state.ledPos = state.width - 1;
      state.changeColor = true;
    } else if (state.ledPos <= 0) {
      state.up = true;
      state.ledPos = 0;
      state.changeColor = true;
    }

    // change color
    if (state.changeColor || Every_LED_Random_Color) {
      if (Random_Color || Every_LED_Random_Color) {
        state.color = colors.random();
      } else if (state.currentColor === state.lastColor - 1) {
        state.color = params.Color[0];
        state.currentColor = 0;
      } else {
        state.currentColor += 1;
        state.color = params.Color[state.currentColor];
      }
      state.changeColor = false;
    }
  },

  params: {
    Color: types.array(
      [
        colors.Red,
        colors.Blue,
        colors.Green,
      ],
      types.rgb(),
      descriptions.Colors
    ),
    Number_Of_Coverups: types.number(1, descriptions.NumberOfAnimations),
    Animation_Speed: types.number(20, descriptions.AnimationSpeed),
    Wrap_Around: types.bool(false, descriptions.WrapAround),
    Random_Color: types.bool(false, descriptions.RandomAnimationColors),
    Every_LED_Random_Color: types.bool(false, descriptions.EveryLedRandomColor),
  },

  name: 'Coverup',
  description: 'Cover the string one light at a time',
};
