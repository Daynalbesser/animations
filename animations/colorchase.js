/* Copyright (c) 2018 ColorBit LLC
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
var colors = require('colors.js');
var types = require('types.js');
var descriptions = require('desc.js');

module.exports = {
  init() {
    state.up = true;
    state.ledPos = 0;
    state.width = LEDS.length / params.Number_Of_Chasers;
    state.color_one_tolerance = params.Width_Color_One;
    state.color_two_tolerance = params.Width_Color_One + params.Width_Color_Two;
    state.color_three_tolerance = state.color_two_tolerance
      + params.Width_Color_Three;
  },

  anim() {
    var i;
    var j;
    var color_one_tolerance = state.color_one_tolerance;
    var color_two_tolerance = state.color_two_tolerance;
    var color_three_tolerance = state.color_three_tolerance;
    var width = state.width;
    var Color_One = params.Color_One;
    var Color_Two = params.Color_Two;
    var Color_Three = params.Color_Three;
    var Wrap_Around = params.Wrap_Around;

    // moving up the string
    if (state.up) {
      for (i = 0; i < color_three_tolerance; i += 1) {
        if ((state.ledPos - i) < 0) {
          // if we are wrapping around, when drawing the block is less than ze-
          // ro, draw the remainder of the block at the end of the string inst-
          // ead of breaking
          if (Wrap_Around) {
            for (j = 0; i + j < color_three_tolerance; j += 1) {
              if (width - j <= state.ledPos) {
                break;
              } else if (i + j < color_one_tolerance) {
                LEDS[width - j - 1] = Color_One;
              } else if (i + j < color_two_tolerance) {
                LEDS[width - j - 1] = Color_Two;
              } else {
                LEDS[width - j - 1] = Color_Three;
              }
            }
          }
          break;
        } else if (i < color_one_tolerance) {
          LEDS[state.ledPos - i] = Color_One;
        } else if (i < color_two_tolerance) {
          LEDS[state.ledPos - i] = Color_Two;
        } else {
          LEDS[state.ledPos - i] = Color_Three;
        }
      }
    } else {
      // moving down the string
      for (i = 0; i < color_three_tolerance; i += 1) {
        if ((state.ledPos + i) > width) {
          break;
        } else if (i < color_one_tolerance) {
          LEDS[state.ledPos + i] = Color_One;
        } else if (i < color_two_tolerance) {
          LEDS[state.ledPos + i] = Color_Two;
        } else {
          LEDS[state.ledPos + i] = Color_Three;
        }
      }
    }

    mirror(LEDS, width);
    show(LEDS);
    fade_all_to_black(LEDS, params.Fade_Speed);
    delay(params.Animation_Speed);

    if (state.up) {
      state.ledPos += 1;
    } else {
      state.ledPos -= 1;
    }

    // if wrap around, start back at the beginning of the string
    if (Wrap_Around) {
      if (state.ledPos >= width) {
        state.ledPos = 0;
      }
    } else if (state.ledPos >= width) {
      // if not wrap around, change from up to down or vice versa
      state.up = false;
      state.ledPos = state.width - 1;
    } else if (state.ledPos <= 0) {
      state.up = true;
      state.ledPos = 0;
    }
  },

  params: {
    Color_One: types.rgb(colors.Red, descriptions.Colors),
    Width_Color_One: types.number(3, 'Number of LEDs For Color 1'),
    Color_Two: types.rgb(colors.Green, descriptions.Colors),
    Width_Color_Two: types.number(3, 'Number of LEDs For Color 2'),
    Color_Three: types.rgb(colors.Blue, descriptions.Colors),
    Width_Color_Three: types.number(3, 'Number of LEDs For Color 3'),
    Number_Of_Chasers: types.number(1, descriptions.NumberOfAnimations),
    Animation_Speed: types.number(25, descriptions.AnimationSpeed),
    Fade_Speed: types.number(20, descriptions.FadeToBlackSpeed),
    Wrap_Around: types.bool(false, descriptions.WrapAround),
  },

  name: 'Chasing Blocks of Colors',
  description: 'A block of colors chasing with a tail',
};
