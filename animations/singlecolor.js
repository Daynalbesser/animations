/* Copyright (c) 2018 ColorBit LLC
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
var colors = require('colors.js');
var types = require('types.js');
var descriptions = require('desc.js');

module.exports = {
  init() {
    state.init = false;
  },

  anim() {
    if (!state.init) {
      LEDS[0] = params.Color;
      mirror(LEDS, 1);
      show(LEDS);
      state.init = true;
    }
  },

  params: {
    Color: types.rgb(colors.Red, descriptions.Colors),
  },

  name: 'Single Color',
  description: 'Fill the string with a single color',
};
