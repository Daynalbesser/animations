/* Copyright (c) 2018 ColorBit LLC
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
var colors = require('colors.js');
var types = require('types.js');
var descriptions = require('desc.js');

module.exports = {
  init() {
    state.width = LEDS.length / params.Number_Of_Pulses;
    state.midpoint = state.width / 2;
    state.peak = state.midpoint * 0.8;
    state.loud = state.midpoint * 0.5;
  },

  anim() {
    var i;
    var peak = state.peak;
    var loud = state.loud;
    var Peak_Color = params.Peak_Color;
    var Loud_Color = params.Loud_Color;
    var Normal_Color = params.Normal_Color;
    var midpoint = state.midpoint;

    fade_all_to_black(LEDS, params.Fade_Speed);

    const num_on = beatsin16(params.BPM, 0, state.midpoint);

    for (i = 0; i <= num_on; i += 1) {
      if (i > peak) {
        LEDS[midpoint + i] = Peak_Color;
        LEDS[midpoint - i] = Peak_Color;
      } else if (i > loud) {
        LEDS[midpoint + i] = Loud_Color;
        LEDS[midpoint - i] = Loud_Color;
      } else {
        LEDS[midpoint + i] = Normal_Color;
        LEDS[midpoint - i] = Normal_Color;
      }
    }

    mirror(LEDS, state.width);
    show(LEDS);
  },
  params: {
    Normal_Color: types.rgb(colors.Green, descriptions.Colors),
    Loud_Color: types.rgb(colors.Yellow, descriptions.Colors),
    Peak_Color: types.rgb(colors.Red, descriptions.Colors),
    Number_Of_Pulses: types.number(2, descriptions.NumberOfAnimations),
    BPM: types.number(60, 'Number of pulses (beats) per minute.'),
    Fade_Speed: types.number(10, descriptions.FadeToBlackSpeed),
  },

  name: 'Pulse (VU Meter)',
  description: 'Pulsing lights to beats per minute.',
};
