#!/bin/bash

IP=$1
FILE=$2

export NODE_PATH="modules/:animations/"
NAME=$(node -e "console.log(require('$(basename "${FILE}")').name)")
ANIM=$(curl -s "http://${IP}/anim" |
    jq -r ".animations | .[] | select(.name == \"${NAME}\") | .file")


echo -n "Stopping any running animation... "
curl -XPOST "http://${IP}/anim/stop"

if [ -n "${ANIM}" ]; then
    for anim in $ANIM; do
        echo "Deleting ${NAME} (${anim})"
        curl -XDELETE "http://${IP}/anim/${anim}"
    done
fi

echo -n "Uploading animation: ${FILE}... "
curl -XPOST --data-binary "@${FILE}" "http://${IP}/anim"

ANIM=$(curl -s "http://${IP}/anim" |
    jq -r ".animations | .[] | select(.name == \"${NAME}\") | .file")

echo "New animation ID: ${ANIM}"
read -p "Play new animation? " -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]
then
    echo
    curl -XPOST "http://${IP}/anim/${ANIM}"
fi
